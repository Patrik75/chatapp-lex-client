import React from 'react'
import '../styles/footer.css'

export const Footer = () => {
    return (
        <footer>
            <div className="container">
                <p className="copyright">©Hospodarsky Developeri 2021</p>
            </div>
        </footer>
    )
}
