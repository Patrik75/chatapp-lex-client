import React from 'react'
import '../styles/header.css'

export const Header = () => {
    return (
        <header>
            <div className="header_image">
                <section className="hero">
                    <div className="container">
                        <h1>Pandemic Bot</h1>
                    </div>
                </section>
            </div>
        </header>
    )
}
